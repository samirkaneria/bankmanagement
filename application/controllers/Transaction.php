<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

	public function processTransaction(){
		$this->load->model("transaction_model");
		$this->load->model("account_model");
		$receiverCustID = $this->input->post("to_custid");
		$senderCustID = $this->session->userdata("custID");

		$receiverRow = $this->account_model->getAccount($receiverCustID);
		$senderRow = $this->account_model->getAccount($senderCustID);

		$senderID = $senderRow["id"];
		$receiverID = $receiverRow["id"];
		$amount = $this->input->post("amount");
		$this->transaction_model->debit_amount($senderID,$amount);
		$this->transaction_model->credit_amount($receiverID,$amount);
		$this->transaction_model->log_transaction($amount,$senderID,$receiverID);

		$output = array(
			"status" => "success"
		);
		echo json_encode($output);
	}

	public function getHistory($accountID){
		$this->load->model("transaction_model");
		$history = $this->transaction_model->getHistory($accountID);

		$output = [];
		foreach ($history as $key => $transactionRow) {
			if($transactionRow["sender_id"] == $accountID){
				$type = "Debit";
			}else{
				$type = "Credit";
			}
			$transactionRow["type"] = $type;
			$output[] = $transactionRow;
		}
		return $output;
	}
}