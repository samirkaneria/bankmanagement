<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactionhistory extends CI_Controller {

	public function index()
	{

		$this->load->model("transaction_model");
		$this->load->model("account_model");
		$custID = $this->session->userdata("custID");
		$accountData = $this->account_model->getAccount($custID);
		$accountID = $accountData["id"];
		$history = $this->transaction_model->getHistory($accountID);
		
		$viewData = array(
			"fullname" => $this->session->userdata("userid"),
			"transactions" => $history,
			"account" => $accountData
		);
		$this->load->view("transactionhistory",$viewData);
	}

	public function getAccount($customerID){
		$this->load->model("account_model");
		$output = $this->account_model->getAccount($customerID);
		echo json_encode($output);
	}

	public function getSessionData(){
		echo $this->session->userdata("email");
	}
}