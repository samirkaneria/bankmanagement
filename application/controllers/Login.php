<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login');
	}

	public function checkLogin(){
		$this->load->model("account_model");
		$custID = $this->input->post("cust_id");
		$pass = $this->input->post("pass");
		$result = $this->account_model->checkLogin($custID,$pass);
		if($result  == "success"){
			//if its valid
			$newdata = array(
		        'custID'  => $custID
			);
			$this->session->set_userdata($newdata);	
		}
		$output = array(
			"status" => $result
		);
		echo json_encode($output);

		
	}
}
