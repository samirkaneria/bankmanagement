<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->load->model("account_model");
		$this->load->model("transaction_model");
		if($this->session->has_userdata("custID")){

			$custID = $this->session->userdata("custID");
			$accountData = $this->account_model->getAccount($custID);
			$viewData = array(
				"account" => $accountData,
				"totalTransactions" => $this->transaction_model->transactionCount($accountData["id"]),
				"totalCredited" => $this->transaction_model->totalCredited($accountData["id"]),
				"totalDebited" => $this->transaction_model->totalDebited($accountData["id"]),
				"sentAmounts" => $this->transaction_model->sentAmounts($accountData["id"]),
				"recievedAmounts" => $this->transaction_model->recievedAmounts($accountData["id"]),
			);
			$this->load->view("home",$viewData);
		}else{
			redirect("/login");
		}
		
	}

	public function getAccount($customerID){
		$this->load->model("account_model");
		$output = $this->account_model->getAccount($customerID);
		echo json_encode($output);
	}

	public function getSessionData(){
		echo $this->session->userdata("email");
	}
}