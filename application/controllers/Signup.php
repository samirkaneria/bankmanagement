<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('signup');
	}

	public function register(){
		$this->load->model("account_model");
		$formInput = $this->input->post("formInput");
		$insertData = array(
			"first_name" => $formInput["first_name"],
			"last_name" => $formInput["last_name"],
			"email" => $formInput["email"],
			"password" => $formInput["password"]
		);
		$custID = $this->account_model->createAccount($insertData);
		$newdata = array(
		    'custID'  => $custID
		);
		$this->session->set_userdata($newdata);	
	}
}
