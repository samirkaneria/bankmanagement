<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fundtransfer extends CI_Controller {

	public function index()
	{
		$this->load->model("account_model");
		if($this->session->has_userdata("custID")){

			$custID = $this->session->userdata("custID");
			$accountData = $this->account_model->getAccount($custID);
			$viewData = array(
				"fullname" => $this->session->userdata("userid"),
				"account" => $accountData
			);
			$this->load->view("fundtransfer",$viewData);
		}
	}

	public function getAccount($customerID){
		$this->load->model("account_model");
		$output = $this->account_model->getAccount($customerID);
		echo json_encode($output);
	}

	public function getSessionData(){
		echo $this->session->userdata("email");
	}
}