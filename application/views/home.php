<!DOCTYPE html>
<html>
<head>
	<title>banking system</title>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<!-- Compiled and minified CSS -->
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    	<!-- Compiled and minified JavaScript -->
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    	<script src="http://localhost/bank/html/assets/js/script.js" type="text/javascript"></script>
    	<link rel="stylesheet" href="http://localhost/bank/html/assets/css/style.css">
    	<script type="text/javascript">
    		$(document).ready(function(){
			    $('.tabs').tabs();
			});
    	</script>
</head>
<body>
	<div class="navbar-fixed">
	<nav>
	    <div class="nav-wrapper">
	      	<div class="brand-logo-container left">
	      		<div class="logo-top">Bank Of</div>
	      		<div class="logo-bottom">SAL</div>
	      	</div>
		    <ul id="nav-mobile" class="right">
		        <li><a href="http://localhost/bank/logout">Logout</a></li>
		    </ul>
	    </div>
	</nav>
</div>
        
	<!-- <div class="row header">
		<div class="row welcome-text">
			WELCOME TO SAL EDUCATION BANK!	
		</div>
		<div class="row headerbtns">
			<div class="col s6 welcome-user">
				Welcome <?php echo $account["first_name"]; ?>
			</div>
			<div class="col s6 profilebtn">
				<a id="logout" class="btn grey darken-4 grey-text text-lighten-5 waves-effect waves-light">Logout</a>
			</div>			
		</div>		
	</div> -->
	<div class="row maincontent">
		<div id="menuContainer" class="col s2 a z-depth-1">
			<a style="border-top: 1px solid #f1f1f1;" class="menu-item selected" href="http://localhost/bank/home">Summary</a>
			<a class="menu-item" href="http://localhost/bank/fundtransfer">Fund Transfer</a>
			<a class="menu-item" href="http://localhost/bank/transactionhistory">Transaction History</a>
			<a class="menu-item" href="http://localhost/bank/contactus">Send Query</a>
		</div>
		<div class="col s10 b">
			<div class="row">
				<div id="personalDetails" class="col s6">
					<div class="detail-row">
						<div class="detail-label">Fullname</div>
						<div class="detail-value"><?php echo $account["first_name"]." ".$account["last_name"]; ?></div>
					</div>
					<div class="detail-row">
						<div class="detail-label">Email</div>
						<div class="detail-value"><?php echo $account["email"]; ?></div>
					</div>
					<div class="detail-row">
						<div class="detail-label">Member since</div>
						<div class="detail-value"><?php echo $account["create_date"]; ?></div>
					</div>
					<div class="detail-row">
						<div class="detail-label">Account No</div>
						<div class="detail-value"><?php echo $account["customer_id"]; ?></div>
					</div>
					<div class="detail-row">
						<div class="detail-label">Current Balance</div>
						<div class="detail-value"><?php echo $account["current_balance"]; ?></div>
					</div>
				</div>
				<div id="transactionStats" class="col s6">
					<div class="detail-row">
						<div class="detail-label">Total Transactions</div>
						<div class="detail-value"><?php echo $totalTransactions; ?></div>
					</div>
					<div class="detail-row">
						<div class="detail-label">Total Amount Credited</div>
						<div class="detail-value"><?php echo $totalCredited; ?></div>
					</div>
					<div class="detail-row">
						<div class="detail-label">Total Amount Debited</div>
						<div class="detail-value"><?php echo $totalDebited; ?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div id="transactionTabs" class="card-panel">
					<div class="row">
					    <div class="col s12">
					      <ul class="tabs">
					        <li class="tab col s6"><a href="#fundsSent">Sent</a></li>
					        <li class="tab col s6"><a class="active" href="#fundsReceived">Received</a></li>
					      </ul>
					    </div>
					    <div id="fundsSent" class="col s12">
							<table  class=" highlight responsive-table">
						        <thead>
						          <tr>
						              <th  style="width:25%; text-align: center;">Transaction ID</th>
						              <th style="width:25%; text-align: center;">Amount</th>
						              <th style="width:25%; text-align: center;">To</th>
						              <th style="width:25%; text-align: center;">Transaction Time</th>
						          </tr>
						        </thead>
						        <tbody>
						        	<?php foreach ($sentAmounts as $key => $transactionRow) { ?>
						        	<tr>
							            <td style="width:25%; text-align: center;"><?php echo md5($transactionRow["id"]); ?></td>
							            <td style="width:25%; text-align: center;"><?php echo $transactionRow["amount"]; ?></td>
							            <td style="width:25%; text-align: center;"><?php echo $transactionRow["first_name"]." ".$transactionRow["last_name"]; ?></td>
							            <td style="width:25%; text-align: center;"><?php echo $transactionRow["transaction_date"]; ?></td>
							         </tr>
						        	<?php } ?>
						        </tbody>
						    </table>
					    </div>
					    <div id="fundsReceived" class="col s12">
					    	

						  	<table  class=" highlight responsive-table">
						        <thead>
						          <tr>
						              <th  style="width:25%; text-align: center;">Transaction ID</th>
						              <th style="width:25%; text-align: center;">Amount</th>
						              <th style="width:25%; text-align: center;">From</th>
						              <th style="width:25%; text-align: center;">Transaction Time</th>
						          </tr>
						        </thead>
						        <tbody>
						        	<?php foreach ($recievedAmounts as $key => $transactionRow) { ?>
						        	<tr>
							            <td style="width:25%; text-align: center;"><?php echo md5($transactionRow["id"]); ?></td>
							            <td style="width:25%; text-align: center;"><?php echo $transactionRow["amount"]; ?></td>
							            <td style="width:25%; text-align: center;"><?php echo $transactionRow["first_name"]." ".$transactionRow["last_name"]; ?></td>
							            <td style="width:25%; text-align: center;"><?php echo $transactionRow["transaction_date"]; ?></td>
							         </tr>
						        	<?php } ?>
						        </tbody>
						    </table>

					    </div>
					</div>
				</div>
			</div>

		</div>		
	</div>
</body>
</html>