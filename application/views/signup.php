<!DOCTYPE html>
<html>
<head>
	<title>banking system</title>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<!-- Compiled and minified CSS -->
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    	<!-- Compiled and minified JavaScript -->
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    	<script src="http://localhost/bank/html/assets/js/script.js" type="text/javascript"></script>
    	<link rel="stylesheet" href="http://localhost/bank/html/assets/css/style.css">
    	<script type="text/javascript">
    		function signIn(){
    			var firstName = $("#firstName").val();
    			var lastName = $("#lastName").val();
    			var pass = $("#bankpassword").val();
    			var pass2 = $("#bankpassword2").val();
    			var email = $("#email").val();
    			if(pass == pass2){
    				var formInput = {
    					"first_name" : firstName,
    					"last_name" : lastName,
    					"password" : pass,
    					"email" : email
    				};
    				$.post("http://localhost/bank/signup/register",{formInput:formInput},function(data){
    					alert("account created");
    					window.location.href="http://localhost/bank/home";
    				});
    			}else{
    				alert("Password Mismatch!");
    			}
    		}
    	</script>
</head>
<body>
	<div class="signup heading">
		Signup using your bank details
	</div>
	<div class="signup-container z-depth-1">
		<div class="bankdetails row">
			<input type="text" id="firstName" name="firstName"  placeholder="First name" class="create-form-input"  autocomplete="new-password" >
			<input type="text" id="lastName" name="lastName"  placeholder="Last name" class="create-form-input"  autocomplete="new-password" >
			<!-- <input type="text" name="customerid"  placeholder="Customer id" class="customerid"  autocomplete="new-password" >
			<input type="text" name="accountno"  placeholder="Account No" class="accountno"  autocomplete="new-password" >
			<input type="text" name="ifsccode"  placeholder="IFSC Code" class="ifsccode"  autocomplete="new-password" >
			<input type="text" name="contactno"  placeholder="Contact No" class="contactno"  autocomplete="new-password" > -->
			<input type="text" id="email" name="email"  placeholder="Email ID" class="email"  autocomplete="new-password" >
			<input type="password" id="bankpassword" name="bankpassword" placeholder="Set Password" class="pwd"  autocomplete="new-password" >
			<input type="password" id="bankpassword2" name="bankpassword2" placeholder="Confirm Password" class="pwd"  autocomplete="new-password" >
		</div>
		<div class="signinbutn">
			<a href="#!" onclick="signIn();" class="btn grey darken-4 grey-text text-lighten-5 waves-effect waves-light btn-block">Signup</a>
		</div>
	</div>

</body>
</html>