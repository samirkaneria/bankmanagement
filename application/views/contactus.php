<!DOCTYPE html>
<html>
<head>
	<title>banking system</title>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<!-- Compiled and minified CSS -->
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    	<!-- Compiled and minified JavaScript -->
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    	<script src="http://localhost/bank/html/assets/js/script.js" type="text/javascript"></script>
    	<link rel="stylesheet" href="http://localhost/bank/html/assets/css/style.css">
</head>
<body>
	<div class="navbar-fixed">
		<nav>
		    <div class="nav-wrapper">
		      	<div class="brand-logo-container left">
		      		<div class="logo-top">Bank Of</div>
		      		<div class="logo-bottom">SAL</div>
		      	</div>
			    <ul id="nav-mobile" class="right">
			        <li><a href="http://localhost/bank/logout">Logout</a></li>
			    </ul>
		    </div>
		</nav>
	</div>
        
	<!-- <div class="row header">
		<div class="row welcome-text">
			WELCOME TO SAL EDUCATION BANK!	
		</div>
		<div class="row headerbtns">
			<div class="col s6 welcome-user">
				Welcome <?php echo $account["first_name"]; ?>
			</div>
			<div class="col s6 profilebtn">
				<a id="logout" class="btn grey darken-4 grey-text text-lighten-5 waves-effect waves-light">Logout</a>
			</div>			
		</div>		
	</div> -->
	<div class="row maincontent">
		<div id="menuContainer" class="col s2 a z-depth-1">
			<a style="border-top: 1px solid #f1f1f1;" class="menu-item" href="http://localhost/bank/home">Summary</a>
			<a class="menu-item " href="http://localhost/bank/fundtransfer">Fund Transfer</a>
			<a class="menu-item " href="http://localhost/bank/transactionhistory">Transaction History</a>
			<a class="menu-item selected" href="http://localhost/bank/contactus">Send Query</a>
		</div>
		<div class="col s10 querytext">
			<div class="card-panel">
	 			<blockquote>
      				For any query or information contact us on saleducationbank@gmail.com.
    			</blockquote>
				<blockquote> 
					We will give response in less than 24 hours.
				</blockquote>
			</div>
		</div>
	</div>
</body>
</html>