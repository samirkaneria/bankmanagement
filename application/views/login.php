<!DOCTYPE html>
<html>
<head>
	<title>banking system</title>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<!-- Compiled and minified CSS -->
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    	<!-- Compiled and minified JavaScript -->
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    	<script src="http://localhost/bank/html/assets/js/script.js" type="text/javascript"></script>
    	<link rel="stylesheet" href="http://localhost/bank/html/assets/css/style.css">
</head>
<body>
	<div class="heading">
		Bank Management System
	</div>
	<div class="login-container z-depth-1">
		<div class="idandpass row">

			<input type="text" id="customerid" name="customerid"  placeholder="Customer id" class="customerid"  autocomplete="new-password" >
			<input type="password" id="bankpassword" name="bankpassword" placeholder="Password" class="pwd"  autocomplete="new-password" >
		</div>
		<div class="loginbutn">
			<a onclick="checkLogin();" id="loginBtn" class="btn grey darken-4 grey-text text-lighten-5 waves-effect waves-light">LOGIN</a>
		</div>
		<div class="ortext">
			OR
		</div>
		<div class="signupBtn">
			<a href="http://localhost/bank/signup" id="signupBtn" class="btn grey darken-4 grey-text text-lighten-5 waves-effect waves-light">Signup</a>
		</div>
	</div>
	

</body>
</html>