<?php 

class Account_model extends CI_Model{
		
	function createAccount($accountData)
	{
		$query = $this->db->insert("accounts",$accountData);
		$insertID = $this->db->insert_id();
		$updateData = array(
			"customer_id" => time().str_pad($insertID, 4, '0', STR_PAD_LEFT)
		);
		$this->db->where("id",$insertID);
		$this->db->update("accounts",$updateData);
		return $updateData["customer_id"];
	}	

	function getAccount($customer_id){
		$query = $this->db->query("select * from accounts where customer_id='$customer_id'");
		$result = $query->row_array();
		return $result;
	}

	function getAccountByAccountId($account_id){
		$query = $this->db->query("select * from accounts where id='$account_id'");
		$result = $query->row_array();
		return $result;
	}

	function checkLogin($customer_id,$pass){
		$query = $this->db->query("select * from accounts where customer_id='$customer_id' and password='$pass'");
		if($query->num_rows() == 1){
			return "success";
		}else{
			return "fail";
		}
	}

}