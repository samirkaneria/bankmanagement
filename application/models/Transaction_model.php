<?php 

class Transaction_model extends CI_Model{
		
	function debit_amount($id,$amount){
		$this->db->set('current_balance', 'current_balance-'.$amount, false);
	    $this->db->where('id' , $id);
	    $this->db->update('accounts');
	}

	function credit_amount($id,$amount){
		$this->db->set('current_balance', 'current_balance+'.$amount, false);
	    $this->db->where('id' , $id);
	    $this->db->update('accounts');
	}

	function log_transaction($amount,$senderID,$receiverID){
		$this->load->model("account_model");
		$accountRow = $this->account_model->getAccountByAccountId($senderID);
		$senderInsert = array(
			"account_id" => $senderID,
			"transaction_account_id" => $receiverID,
			"type" => "Debit",
			"amount" => $amount,
			"balance" => $accountRow["current_balance"],
			"transaction_date" => date("Y-m-d H:i:s")
		);
		$this->db->insert("transaction_history",$senderInsert);

		$accountRow = $this->account_model->getAccountByAccountId($receiverID);
		$receiverInsert = array(
			"account_id" => $receiverID,
			"transaction_account_id" => $senderID,
			"type" => "Credit",
			"amount" => $amount,
			"balance" => $accountRow["current_balance"],
			"transaction_date" => date("Y-m-d H:i:s")
		);
		$this->db->insert("transaction_history",$receiverInsert);
	}

	function getHistory($accountID){
		$query = $this->db->query("select th.*,a.first_name,a.last_name from transaction_history th join accounts a on a.id=th.transaction_account_id where th.account_id='$accountID' order by th.id DESC");
		$result = $query->result_array();
		return $result;
	}

	function transactionCount($accountID){
		$query = $this->db->query("select count(*) as  transactions from transaction_history where account_id='$accountID'");
		$result = $query->row_array();
		return $result["transactions"];
	}

	function totalCredited($accountID){
		$query = $this->db->query("select sum(amount) as creditedTotal from transaction_history where account_id='$accountID' and type='Credit'");
		$result = $query->row_array();
		return $result["creditedTotal"];
	}

	function totalDebited($accountID){
		$query = $this->db->query("select sum(amount) as debitedTotal from transaction_history where account_id='$accountID' and type='Debit'");
		$result = $query->row_array();
		return $result["debitedTotal"];
	}

	function sentAmounts($accountID){
		$query = $this->db->query("select th.*,a.first_name,a.last_name from transaction_history th join accounts a on a.id=th.transaction_account_id where th.account_id='$accountID' and th.type='Debit' order by th.id DESC");
		$result = $query->result_array();
		return $result;
	}

	function recievedAmounts($accountID){
		$query = $this->db->query("select th.*,a.first_name,a.last_name from transaction_history th join accounts a on a.id=th.transaction_account_id where th.account_id='$accountID' and th.type='Credit' order by th.id DESC");
		$result = $query->result_array();
		return $result;
	}

}