-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 05, 2019 at 02:20 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bankmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `current_balance` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `customer_id`, `first_name`, `last_name`, `password`, `email`, `create_date`, `update_date`, `current_balance`) VALUES
(1, '15534202170001', 'Rajan', 'Kaneria', 'rajan123', 'rajan.kaneria@gmail.com', '2019-03-24 15:06:57', '2019-03-24 15:06:57', 969480),
(2, '15534202680002', 'Samir', 'Kaneria', 'sam123', 'samirkaneria3799@gmail.com', '2019-03-24 15:07:48', '2019-03-24 15:07:48', 29320),
(3, '15544160350003', 'Cooper', 'Patel', 'cooper123', 'cooper@patel.com', '2019-04-05 03:43:55', '2019-04-05 03:43:55', 1200);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `transaction_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `sender_id`, `receiver_id`, `amount`, `transaction_date`) VALUES
(1, 1, 2, 2500, '2019-04-01 03:14:55'),
(2, 1, 2, 25000, '2019-04-01 03:15:28'),
(3, 2, 1, 1000, '2019-04-01 03:16:08');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

DROP TABLE IF EXISTS `transaction_history`;
CREATE TABLE IF NOT EXISTS `transaction_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `transaction_account_id` int(11) NOT NULL,
  `type` enum('Credit','Debit') NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `transaction_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_history`
--

INSERT INTO `transaction_history` (`id`, `account_id`, `transaction_account_id`, `type`, `amount`, `balance`, `transaction_date`) VALUES
(1, 2, 1, 'Debit', 1000, 24500, '2019-04-03 19:09:38'),
(2, 1, 2, 'Credit', 1000, 975500, '2019-04-03 19:09:38'),
(3, 1, 2, 'Debit', 500, 975000, '2019-04-03 19:12:11'),
(4, 2, 1, 'Credit', 500, 25000, '2019-04-03 19:12:11'),
(5, 1, 2, 'Debit', 500, 974500, '2019-04-03 19:13:35'),
(6, 2, 1, 'Credit', 500, 25500, '2019-04-03 19:13:35'),
(7, 2, 1, 'Debit', 200, 25300, '2019-04-04 00:45:27'),
(8, 1, 2, 'Credit', 200, 974700, '2019-04-04 00:45:27'),
(9, 1, 3, 'Debit', 1250, 973450, '2019-04-05 03:44:54'),
(10, 3, 1, 'Credit', 1250, 1250, '2019-04-05 03:44:54'),
(11, 3, 2, 'Debit', 50, 1200, '2019-04-05 03:47:11'),
(12, 2, 3, 'Credit', 50, 25350, '2019-04-05 03:47:11'),
(13, 1, 2, 'Debit', 1470, 971980, '2019-04-05 07:42:20'),
(14, 2, 1, 'Credit', 1470, 26820, '2019-04-05 07:42:20'),
(15, 1, 2, 'Debit', 2500, 969480, '2019-04-05 07:46:35'),
(16, 2, 1, 'Credit', 2500, 29320, '2019-04-05 07:46:35');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
